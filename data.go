// Copyright 2023-2024 JetERA Creative
// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0
// that can be found in the LICENSE file and https://mozilla.org/MPL/2.0/.

package vcdp

import "math/big"

type Variable struct {
	Name string

	Unit     uint
	BitWidth uint

	Timestamps   []uint
	ValueChanges []big.Int
}

func (v *Variable) GetValueAt(index int) big.Int {
	return v.ValueChanges[index]
}

func (v *Variable) GetIndexAtTimestamp(startIndex int, timestamp uint) int {
	return BinarySearch(v.Timestamps, timestamp, 0, len(v.Timestamps)-1, 0)
}
