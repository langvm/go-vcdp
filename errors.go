// Copyright 2024 LangVM Project
// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0
// that can be found in the LICENSE file and https://mozilla.org/MPL/2.0/.

package vcdp

import (
	"fmt"
)

type EOFError struct {
	Pos Position
}

func (e EOFError) Error() string {
	return "EOF"
}

type FormatError struct {
	Pos Position
}

func (e FormatError) Error() string {
	return fmt.Sprintln(e.Pos.String(), "format error")
}
