// Copyright 2023-2024 JetERA Creative
// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0
// that can be found in the LICENSE file and https://mozilla.org/MPL/2.0/.

package vcdp

type Parser struct {
	Scanner
}

func (p *Parser) ParseVarDef(scope string) (string, Variable) {
}

func (p *Parser) ParseScope() {}

func (p *Parser) ParseHeader() {}

func (p *Parser) ParseDump() {
}

type DumpParser struct {
	Scanner
}

func (p *DumpParser) ScanVal() ([]uint8, string) {
}
