// Copyright 2024 LangVM Project
// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0
// that can be found in the LICENSE file and https://mozilla.org/MPL/2.0/.

package vcdp

import (
	"errors"
	"fmt"
	"math/big"
	"unicode"
)

type Position struct {
	Offset, Line, Column int
}

func (p Position) String() string {
	return fmt.Sprint(p.Offset, ":", p.Line, ":", p.Column)
}

type BufferScanner struct {
	Position // Cursor
	Buffer   []rune
}

// Move returns current char and move cursor to the next.
// Move does not error when GetChar does not error.
func (bs *BufferScanner) Move() (rune, error) {
	ch, err := bs.GetChar()
	if err != nil {
		return 0, err
	}

	if ch == '\n' {
		bs.Column = 0
		bs.Line++
	} else {
		bs.Column++
	}

	bs.Offset++

	return ch, nil
}

// GetChar returns the char at the cursor.
func (bs *BufferScanner) GetChar() (rune, error) {
	if bs.Offset == len(bs.Buffer) {
		return 0, EOFError{Pos: bs.Position}
	}
	return bs.Buffer[bs.Offset], nil
}

// Scanner is the token scanner.
type Scanner struct {
	BufferScanner
}

func (s *Scanner) GotoNextLine() error {
	for {
		ch, err := s.GetChar()
		if err != nil {
			return err
		}
		if ch == '\n' {
			return nil
		}
		_, err = s.Move()
		if err != nil {
			return err
		}
	}
}

func (s *Scanner) SkipWhitespace() error {
	for {
		ch, err := s.GetChar()
		if err != nil {
			return err
		}
		switch ch {
		case ' ':
		case '\t':
		case '\n':
		case '\r':
		default:
			return nil
		}
		_, err = s.Move()
		if err != nil {
			return err
		}
	}
}

func (s *Scanner) ScanToken() (string, error) {
	err := s.SkipWhitespace()
	if err != nil {
		return "", err
	}
	begin := s.Offset
	end := begin

	ch, err := s.Move()
	if err != nil {
		return "", err
	}
	for !unicode.IsSpace(ch) {
		end++
	}

	return string(s.Buffer[begin:end]), nil
}

func (s *Scanner) ScanIdent() (string, error) {
	err := s.SkipWhitespace()
	if err != nil {
		return "", err
	}

	var seq []rune

	for {
		ch, err := s.GetChar()
		if err != nil {
			return "", err
		}
		if unicode.IsSpace(ch) {
			break
		}
		ch, err = s.Move()
		if err != nil {
			return "", err
		}
		seq = append(seq, ch)
	}

	return string(seq), nil
}

func (s *Scanner) ScanWhile(cond func() bool) ([]rune, error) {
	var seq []rune

	for cond() {
		ch, err := s.GetChar()
		if err != nil {
			return nil, err
		}
		seq = append(seq, ch)
		_, err = s.Move()
		if err != nil {
			return nil, err
		}
	}

	if len(seq) == 0 {
		return nil, FormatError{Pos: s.Position}
	}

	return seq, nil
}

func (s *Scanner) ScanValue() (*big.Int, error) {
	v := big.NewInt(0)
	ch, err := s.GetChar()
	if err != nil {
		return nil, err
	}
	switch ch {
	case '0':
		return big.NewInt(0), nil
	case '1':
		return big.NewInt(1), nil
	case 'b':
		for {
			ch, err = s.GetChar()
			if err != nil {
				return nil, err
			}
			v.Lsh(v, 1)
			switch ch {
			case '1':
				v = v.Or(v, big.NewInt(1))
			case '0':
			default:
				return nil, errors.New("unexpected format")
			}
		}
	default:
		panic("unexpected format")
	}
}
