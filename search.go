// Copyright 2023-2024 JetERA Creative
// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0
// that can be found in the LICENSE file and https://mozilla.org/MPL/2.0/.

package vcdp

func BinarySearch[N int | int8 | int16 | int32 | int64 | uint | uint8 | uint16 | uint32 | uint64](values []N, value N, start, end, i int) int {

	mid := start + (end-start)>>1

	if i == mid {
		return -1
	}

	switch {
	case value < values[mid]:
		return BinarySearch(values, value, start, mid, mid)
	case values[mid] < value:
		return BinarySearch(values, value, mid, end, mid)
	default:
		return mid
	}
}
