// Copyright 2023-2024 JetERA Creative
// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0
// that can be found in the LICENSE file and https://mozilla.org/MPL/2.0/.

package vcdp

import "testing"

func TestBinarySearch(t *testing.T) {
	println(BinarySearch([]int{1, 2, 4, 5, 6}, 3, 0, 4, 0))
}
